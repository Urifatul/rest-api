<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
class Siswa extends REST_Controller {
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database(''); // optional
        $this->load->model('M_Siswa');
        $this->load->library('form_validation');
    }
    function index_get(){
        $id = $this->get('id');
        if ($id == '') {
            $data = $this->M_Siswa->fetch_all();
        } else {
            $data = $this->M_Siswa->fetch_single_data($id);
        }
        $this->response($data, 200);
    }
    function index_post(){
        if ($this->post('name') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'name',
                'message' => 'Isian name tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->post('class_id') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'class_id',
                'message' => 'Isian class_id tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->post('date_birth') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'date_birth',
                'message' => 'Isian date_birth tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->post('gender') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'gender',
                'message' => 'Isian gender tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->post('address') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'address',
                'message' => 'Isian address tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        $data = array(
            'name' => $this->post('name'),
            'class_id' => trim($this->post('class_id')),
            'date_birth' => trim($this->post('date_birth')),
            'gender' => trim($this->post('gender')),
            'address' => trim($this->post('address')),
            'created_at' => date('Y-m-d H:i:s'),  
            'updated_at' => date('Y-m-d H:i:s')  
        );
        $this->M_Siswa->insert_api($data);
        $last_row = $this->db->select('*')->order_by('id', 'desc')->limit(1)->get('siswa')->row();
        $response = array(
            'status' => 'success',
            'data' => $last_row,
            'status_code' => 200,
        );
        return $this->response($response);
    }
    function index_put(){
        $id = $this->put('id');
        $check = $this->M_Siswa->check_data($id);
        if ($check == false) {
            $error = array(
                'status' => 'fail',
                'field' => 'id',
                'message' => 'Data tidak ditemukan!',
                'status_code' => 502
            );
            return $this->response($error);
        }
        if ($this->put('name') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'name',
                'message' => 'Isian name tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->put('class_id') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'class_id',
                'message' => 'Isian class_id tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->put('date_birth') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'date_birth',
                'message' => 'Isian date_birth tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->put('gender') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'gender',
                'message' => 'Isian gender tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        if ($this->put('address') == '') {
            $response = array(
                'status' => 'fail',
                'field' => 'address',
                'message' => 'Isian address tidak boleh kosong',
                'status_code' => 502
            );
            return $this->response($response);
        }
        $data = array(
            'name' => $this->put('name'),
            'class_id' => trim($this->put('class_id')),
            'date_birth' => trim($this->put('date_birth')),
            'gender' => trim($this->put('gender')),
            'address' => trim($this->put('address')),
            'created_at' => date('Y-m-d H:i:s'),  
            'updated_at' => date('Y-m-d H:i:s')  
        );
        $this->M_Siswa->update_data($id, $data);
        $newData = $this->M_Siswa->fetch_single_data($id);
        $response = array(
            'status' => 'success',
            'data' => $newData,
            'status_code' => 200,
        );
        return $this->response($response);
    }   
    function index_delete(){
        $id = $this->delete('id');
        $check = $this->M_Siswa->check_data($id);
        if ($check == false) {
            $error = array(
                'status' => 'fail',
                'field' => 'id',
                'message' => 'Data tidak ditemukan!',
                'status_code' => 502
            );
            return $this->response($error);
        }
        $delete = $this->M_Siswa->delete_data($id);
        $response = array(
            'status' => 'success',
            'data' => null,
            'status_code' => 200,
        );
        return $this->response($response);
    }
}
?>